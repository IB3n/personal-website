document.querySelectorAll("[data-click-forward]").forEach(item => {
  item.addEventListener("click", event => {
    if (event.target.nodeName !== "a") {
      document.getElementById(item.dataset.clickForward).click();
    }
  });
});