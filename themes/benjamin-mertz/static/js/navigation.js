const burgers = document.querySelectorAll("[data-burger-control]");
burgers.forEach(burger => {
  burger.addEventListener("click", () => {
    const navbar = document.getElementById(burger.dataset.burgerControl);
    const isOpen = navbar.classList.contains("navbar--open");
    document.body.style.setProperty("overflow", isOpen ? "initial": "hidden");
    document.getElementById(burger.dataset.burgerControl).classList.toggle("navbar--open");
    burger.querySelector(".burger").classList.toggle("is-open");
  });
});
