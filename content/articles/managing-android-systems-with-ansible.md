+++
title= "Managing android system with ansible"
description = "Managing android system with ansible | Benjamin MERTZ"
+++

# Phone prerequisites

In order to make ansible communication with an android phone we need to: 
* Connect to the phone with ssh
* Execute python code on the phone

We can do those actions with Termux, Termux is an Android terminal emulator and Linux environment app that works directly with no rooting or setup required. you can get it on FDroid (it's important not to get it from the Play Store).

## On the phone 

we need to install an ssh server

```sh
pkg install openssh
pkg install python
```

